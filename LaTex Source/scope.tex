\section{Oscilloscope}
The DAD includes a true differential\footnote{A differential input has two inputs: positive and negative. The positive input is referenced to the negative input, rather than system ground. This means, if you want to measure a signal relative to ground, you will need to explicitly connect the negative input to ground.} 2-channel digital oscilloscope with most of the features you would expect from a modern scope. It has vertical scales from $500\ \mu V/div$ to $5\ V/div$ with an input range of $\pm 25\ V$. The scope input can be either the provided ``flywire'' or the optional Digilent BNC adaptor. The flywires have a $9\ MHz$, $-3\ dB$ bandwidth (i.e.,  how fast or high frequency a signal it can accurately measure); the BNC adaptor improves the bandwidth to $30\ MHz$ at $-3\ dB$. The $9\ MHz$, $-3\ dB$ bandwidth is plenty fast enough for most labs you will take, and certainly fast enough for EE 230 labs. If you have not discussed bandwidth or the $-3\ dB$ point yet, don't worry--you will learn about it in EE 230!
\par
You can view all of the oscilloscope specs on the official \href{https://reference.digilentinc.com/reference/instrumentation/analog-discovery-2/specifications?_ga=2.198351529.769346588.1591023484-347414265.1591023484}{DAD specifications page}.

\subsection{Configuration Workspace}
To open the Scope workspace, click ``Scope'' from either the Welcome window or from the green \textbf{\textcolor{ForestGreen}{+}} icon on the Welcome tab. You should see the Scope workspace shown in Figure \ref{label:scope}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/Scope.png}
	\caption{Scope Default Workspace}
	\centering
	\label{label:scope}
\end{figure}

\begin{itemize}
\item
The top-most toolbar starting with ``Export'' provides quick access to several of the scope's features. We'll cover ``Export,'' ``+XY,'' ``X Cursor,'' and ``Y Cursor,'' but you are encouraged to look through the rest of the options. 
\item
The second toolbar with the ``Single'' and ``Run'' buttons are where you will set up the trigger and start/stop the scope.
\item
The panel on the right is where you will configure the time-base and channels. 
\end{itemize}

\subsection{Channels and Time Base}
The panel on the right of the Scope workspace is your access point for adjusting the time base and per-channel settings. In other words, this section is where you will adjust your horizontal and vertical scale. You can also make adjustments in the view-port by scrolling your mouse wheel, but this method can be inaccurate and frustrating. It is highly recommended that you get comfortable with making adjustments through this panel. 


\subsubsection{Time}
The section under the ``Time'' checkbox (Figure \ref{label:scope_time}) allows you to set your horizontal seconds/division. This is fairly straight forward, but finding a good starting place for the $sec/div$ setting can sometimes be challenging. A good starting place might be to choose a value that gives you about four divisions per period. For example, a $15\ kHz$ signal has a period of $\frac{1}{15\ kHz} = 67 \mu s$, so setting the time base to $15$ or $20$ $\mu s$ is a good starting point. 
\par
Clicking the green arrow pointing down will give you more time based configuration options. ``Average'' and ''Oversample'' can help improve the quality of the capture and might be useful for things like phase measurement. The rest of the options are good at their default, but you are welcome to play around with them. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.4\textwidth]{images/Scope_time_panel.png}
	\caption{Scope Time Configuration Panel}
	\centering
	\label{label:scope_time}
\end{figure}

\subsubsection{Channels}
The channels configuration will mostly be used to enable/disable channels, adjust the offset, and volts/division. You can also adjust the offset by dragging the channel-colored triangle on the \emph{left side} of the view-port (The right side triangle adjusts the trigger point). 
\par
Clicking the gear icon next to the channel name will bring up the channel configuration options show in Figure \ref{label:scope_channel_config}.
\par
\textbf{In general, the only options you will need to worry about are ``Attenuation'' and ``Name''.}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.4\textwidth]{images/Scope_channel_config.png}
	\caption{Scope Channel Configuration Menu}
	\centering
	\label{label:scope_channel_config}
\end{figure}

\bigskip \noindent \textbf{Offset as divisions} \par
This checkbox allows you to adjust the vertical offset of a capture in terms of divisions, rather than volts. For instance, if you have the range set to $1\ V/div$ and the offset set to $2$ divisions, you will offset your waveform up by $2\ V$. It is much more common to see offset in volts rather than divisions. 

\bigskip \noindent \textbf{Noise} \par
This checkbox toggles the display of min/max measured noise. You'll see this noise as a dimmer envelope around the signal you're measuring. Turning this off might produce cleaner plots, but leaving it on will give you a general idea of how much noise is in your signal. 

\bigskip \noindent \textbf{Range Mode} \par
This option changes the ``Range'' value to measure in divisions, rather than full scale. If you have this option selected and you enter $1\ V/div$ in the ``Range'' then each division on the vertical grid will represent $1\ V$, giving a full scale range of $10\ V$. \textit{This is the most common mode and you'll see it as the default on almost all scopes.} 
\par
Full allows you to set the vertical scale as a function of the full range. Entering $1\ V$ with this set to Full will display $1\ V$ over the full display, giving you $\frac{1\ V}{10\ divisions} = 0.1\ V/div$.
\par 
PlusMinus works exactly like Full except you specify the full-scale range as a positive/negative value. Both of these options can be helpful to maximize your screen space and measurement resolution. 

\noindent \textbf{Attenuation} \par
This is the setting you should pay the most attention to in this menu. Scope probes very commonly come in $1X$ and $10X$ configurations. The number indicates how much the probe attenuates the signal it's measuring. For a $10X$ probe, it will reduce the magnitude of the signal by $10$ before measuring it. This allows a wider range of measurement, but reduces absolute resolution and requires probe compensation. Still, $10X$ probes are the most common industry probe because they give you a wider measurement range and tend to have higher input impedance. You can also use other gain values (which you might do if you use a voltage divider or amplifier in your measurement).
\par

If you're using the ``flywires,'' you will want to use $1X$ attenuation. If you're using the BNC adaptor, you'll need to check your probes to see if they are $1X$ or $10X$.

\par
\href{http://web.mit.edu/6.101/www/reference/ABCprobes_s.pdf}{This Tektronix guide} extensively covers probes.

\bigskip \noindent \textbf{Units, Name, and Label} \par
These options allow you to properly format your measurements. This is essential when writing engineering reports.
\par
``Units'' allow you to clarify what is being labeled (for example, it's not uncommon to measure current with a scope). These can also be used with the Math channel, which gives you more reason to properly label the channels. 
\par
``Label'' lets you label a waveform on the view-port, which can be very useful for keeping track of what node your probe is hooked up to. Typically this is used when you have lots of channels or are generating a report. 
\par
``Name'' changes the reference to the channel in the channel panel. This will mostly be used for your own reference. 

\bigskip \noindent \textbf{Coupling} \par
This option is not software adjustable on the DAD and can only be set with jumpers on the BNC adaptor. It allows you to AC couple a signal, removing the DC component. This is very useful in testing and development to measure the AC characteristics of DC signals or power supply rails. 

\bigskip \noindent \textbf{Sample Mode} \par
This setting adjusts how the fixed-sample rate data is processed. Generally, the default ``Average'' mode will be the best option. From the \href{https://reference.digilentinc.com/reference/instrumentation/analog-discovery-2/specifications?_ga=2.198351529.769346588.1591023484-347414265.1591023484}{WaveForms Reference Manual}:
\begin{quote}
    The oscilloscope AD converter works at a fixed frequency. Depending on the time base setting and the size of the oscilloscope buffer, the sampling frequency can be less than the AD conversion frequency. For instance, if the AD conversion frequency is $100 MHz$ ($10 ns$), the buffer size is 8000 samples and the time base is $200 \mu s/division$, then between two samples there will be $25$ AD conversions. The following filters can be applied to the extra conversions:
    \begin{itemize}
    \item Decimate: will record only the Nth AD conversion.
    \item Average: each sample will be calculated as the average of the AD conversions.
    \item Min/Max: each two samples will be calculated as the minimum and maximum value of the
    conversion results.
    \end{itemize}
\end{quote}

\noindent \textbf{Zero} \par
This allows you to offset the oscilloscope's $0\ V$ level. This feature will see very little use. 


\bigskip \noindent \textbf{Export} \par
This button allows you to export the captured data as a file that can be read into a number-processing tool like MATLAB. This can be useful if you want to do signal processing on a captured signal, or simply want more control over plotting the data. 

\subsection{Triggering}
The Trigger toolbar shown in Figure \ref{label:scope_trigger_simple} provides all the basic settings for setting up the scope's trigger. Understanding triggering is essential for design, testing, and analysis of circuits. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/Scope_trigger_simple.png}
	\caption{Scope Basic Trigger Toolbar}
	\centering
	\label{label:scope_trigger_simple}
\end{figure}

The two buttons on the left are for the two capture modes most digital oscilloscopes have. ``Single'' arms single shot capture mode, which captures a single frame after a trigger and leaves that frame displayed. This is essential for analyzing transient responses. ``Run'' is the standard mode of operation--it continually captures new data (dependent on the trigger) and displays it. 

\subsubsection{Capture and Trigger Modes}
The ``Mode'' dropdown menu allows you to select among four capture modes. The dropdown menu to the right of the capture modes lets you select the triggering mode from ``None,'' ``Normal,'' or ``Auto.'' \textbf{In general, you will want to use ``Repeated'' mode with ``Auto'' triggering}; however, if you're having trouble seeing your signal you can change the triggering to ``None,'' which will force capture and display, it just won't lock the signal in place. 
\par
\bigskip \noindent \textbf{Repeated} \par
This is the basic mode and the one that will be used most often. In this mode, the scope captures a frame after being triggered, then displays the frame once all the data points are captured. 

\bigskip \noindent \textbf{Shift} \par
In this mode, the data points are displayed as they are captured. The waveform draws across the screen from left to right until it reaches the right edge, then the waveform starts scanning left across the screen. This mode only works for relatively large time basis. It will automatically switch to Repeated mode below $100\ ms$.

\bigskip \noindent \textbf{Screen} \par
This mode behaves exactly as shift mode, drawing the data points in real time, until it reaches the right edge of the display. Then it returns to the left edge and begins drawing over the previous data points (rather than panning the view like shift does). This mode only works for relatively large time basis. It will automatically switch to Repeated mode below $100\ ms$.

\bigskip \noindent \textbf{Record} \par
This mode allows you to capture very long signals by streaming the data directly over USB. Because of the real-time stream, the sample rate is limited based on the speed of your USB port--around $1\ M\ sample/sec$.

\subsubsection{Trigger Sources}
The ``Source'' menu allows you to select the trigger source. \textbf{Generally, you will trigger from one of your channels, however, you might find it useful to trigger from the Wavegen if you're using it to drive your circuit.} 
\par
The other trigger modes are extremely useful in test and development. For example, in external mode you can synchronize the trigger with a circuit event like assertion of a chip select for a SPI bus. The pattern trigger can be very useful for debugging as well. It allows you to search for a particular pattern of edges, which can enable you to trigger on things like I2C addresses. These modes won't see much use in freshman or sophomore level classes. 

\subsubsection{Trigger Condition}
The scope can trigger on one of three edge conditions: rising, falling, or either. On a rising edge, the scope will begin saving data when there is a low to high transition. On a falling edge, the scope will begin saving data when there is a high to low transition. On either, the scope will begin saving data on either edge. Figures \ref{label:scope_rising} and \ref{label:scope_falling} show the difference between capturing on the rising edge and falling edge of a signal.
\textbf{You will use rising and falling edge the most, but rising is usually a good starting place.}

\begin{figure}[!ht]
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{images/Scope_rising_edge.png}
  \caption{Signal Captured On Rising Edge}
  \label{label:scope_rising}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=.9\textwidth]{images/scope_falling_edge.png}
  \caption{Signal Captured On Falling Edge}
  \label{label:scope_falling}
\end{minipage}
\end{figure}
\FloatBarrier

\subsubsection{Trigger Level}
The trigger level sets the voltage at which the scope will begin saving data. For a sine wave, the middle of the signal (typically $0\ V$) is a good spot, but you will often need to adjust this value--especially if you have a signal with lots of noise. Notice the orange triangle on the right side of Figures \ref{label:scope_rising} and \ref{label:scope_falling} pointing to the voltage level where the signal crosses the $0\ ms$ point. 
\textbf{You can adjust the level by either changing the values in the ``Level'' box or by dragging that channel-colored triangle on the right side of the view-port.}

\subsubsection{Advanced Trigger Options}
Clicking the green arrow on the right side of the trigger toolbar will reveal the advanced trigger options. These options are much less important, especially in lower level classes. They will see a bit of use in engineering practice, however. You should browse through them, and you can read more about them in the ``Help'' tab or the \href{https://s3-us-west-2.amazonaws.com/digilent/resources/instrumentation/waveforms/waveforms-2015_rm.pdf#page10}{Waveforms Reference Guide}. 

\subsection{XY-Mode}
XY-mode allows you to look at one voltage level as a function of another. In other words, it plots one channel on the X-axis and another on the Y-axis. To open the XY view-port, click the ``+XY'' button on the top toolbar. 
\par
There are not a lot of options here, but one nice feature of the DAD that many digital scopes don't have is the ability to select which channel is X and which channel is Y. Figure \ref{label:scope_xy} shows a capture of the scope in XY-mode. Channel 1 is a $15\ kHz$, $1\ V$ amplitude triangle wave; channel 2 is a $15\ kHz$, $2\ V$ amplitude triangle wave. The channels are in phase with each other (i.e., their peaks and zero-crossing occur at the same time). 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/Scope_xy.png}
	\caption{Scope In XY mode. channel 1: $15\ kHz$, $1\ V$ amplitude triangle wave; channel 2: $15\ kHz$, $2\ V$ amplitude triangle wave}
	\centering
	\label{label:scope_xy}
\end{figure}
\FloatBarrier

\subsection{Measurements}
The Scope workspace has a few ways to measure the captured waveforms. \textbf{On the top-right of the view-port are the view and measurement options}--see Figure \ref{label:scope_quick_measurement}. \textbf{These tools are extremely handy for getting quick information about your captured waveform.}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\textwidth]{images/scope_measurements.png}
	\caption{Location of Quick Measurement Tools}
	\centering
	\label{label:scope_quick_measurement}
\end{figure}
% \FloatBarrier

The magnifying glasses allow you to quickly view and navigate the waveform if you're time-shifted or zoomed in. The three middle icons are quick-measurement tools. 
\par
From left to right these are:
\begin{itemize}
    \item Free measure: Displays and measures the time and voltage values for any point on the view-port
    \item Vertical measure: Measures the time and voltage value for any point on the captured waveform
    \item Pulse measure: Measures the period/frequency and width of a pulse at any point on the captured waveform
\end{itemize}

Hovering over the waveform will move the measuring tool. Clicking on the plot will lock the measurement in place. See Figure \ref{label:scope_measurement_pulse} for an example of the Pulse measure tool. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\textwidth]{images/scope_measurement_period.png}
	\caption{Example of Using The Pulse Measure Tool On A $15\ kHz$ Sine Wave}
	\centering
	\label{label:scope_measurement_pulse}
\end{figure}
\FloatBarrier

\subsection{Cursors}
In addition to quick-measurement tools, you can also add horizontal and vertical cursors to the view-port. The cursors are very useful for both measurements and references. \textbf{To add a cursor, click the X or Y buttons located in the bottom left and upper right corners of the view-port}. You can also add them through the X/Y Cursors panels activated from the top menu bar--see Figure \ref{label:scope_cursor_location}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\textwidth]{images/Scope_cursor_location.png}
	\caption{Location Of Cursor Buttons}
	\centering
	\label{label:scope_cursor_location}
\end{figure}

Cursors can be clicked and dragged to any point on the view-port or you can put them in a specfic position by typing it in to the ``Position'' column in the X/Y Cursors panels (on the bottom of the workspace for X and right of the workspace for Y). 
\par
You can also add a ``Delta'' cursor from the X/Y Cursors panel. A Delta cursor has a specified offset from a ``Normal'' cursor. A Delta cursor will always maintain the delta distance from the ``Normal'' cursor it references in the ``Ref'' column. You can move a ``Normal'' cursor and the delta cursor will follow it. This is handy for setting references or visually inspecting wave differences. 

\par
The X Cursors panel has useful measurement information like $\frac{\Delta V}{\Delta t}$, and frequency measurements. Figure \ref{label:scope_x_cursors} shows an example of using the X Cursors panel.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\textwidth]{images/scope_x_cursors.png}
	\caption{X Cursor Measurement Example}
	\centering
	\label{label:scope_x_cursors}
\end{figure}
\FloatBarrier

\subsection{Display Options}
The gear in the top right corner of the view-port provides several display options for changing the appearance of the plot. You can also access this menu by right-clicking anywhere on the view-port. \textbf{It is a good idea to change the color to Light for lab reports.}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.3\textwidth]{images/Scope_formatting.png}
	\caption{view-port Formatting Menu}
	\centering
	\label{label:scope_formatting}
\end{figure}

\begin{itemize}
    \item Add Label: Adds a text box to the view-port
    \item Clear Labels: Removes all labels on the plot
    \item Measure: These are all the same as the icons in Figure \ref{label:scope_quick_measurement}
    \item Color: Change the color scheme between Light and Dark
    \item Plot Width: Change the line width of the captured waveform
    \item Edge Histogram: Changes the width of the histogram on the right side of the view-port (the histogram displays how much a particular voltage level appears in a waveform)
    \item Multiple Scales: Enables a different vertical scale for each channel--\textbf{this is highly recommended if you are using multiple channels}
\end{itemize}

In addition to these options, you can change the color of a channel by clicking the gear icon next to the channel name in the channel panel on the right side of the workspace. 

\subsection{Exporting Raw Data \& Images}
The Export window allows you to export either an image or the raw waveform data for importing into a number-processing tool like MATLAB (allowing you to further process the data or have more control over plotting). 
\par
To open the Export window, either \textbf{click ``Export'' on the top toolbar, or hit \texttt{ctrl + E} (\texttt{CMD + E} on macOS). To export an image, go to the ``Image'' tab and click ``Save.''}
\par
If you notice the image is exporting with the entire workspace attached, change the ``Source'' dropdown menu to ``Acquisition''. If you intend to include the entire workspace, select ``Oscilloscope.''

\begin{figure}[!ht]
	\centering
	\includegraphics[width=.8\textwidth]{images/Export.png}
	\caption{Export Window}
	\centering
	\label{label:export}
\end{figure}

\subsection{Exercises}
\subsubsection{Plot Formatting}\label{exercise:plot_format}
This exercise will give you practice formatting the view-port for exporting clear, report-ready captures. 
\begin{itemize}
    \item Connect the waveform generator to the input of the oscilloscope by placing a jumper wire from one of the waveform generator channels to one of the scope channel's positive inputs. Then connect ground to the scope channel's negative input. 
    \item Set up the waveform to be a $10\ kHz$ sine wave.
    \item Set up the view-port and export an image that looks like the one shown in Figure \ref{label:practice_waveform}. The tool used in the figure is the free measure tool, but you can play around and use whichever one you prefer. 
\end{itemize}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/waveforms_practice_waveform.png}
	\caption{Image To Reproduce For Exercise \ref{exercise:plot_format}}
	\centering
	\label{label:practice_waveform}
\end{figure}

\newpage
\subsubsection{Measuring Rise Time and Overshoot}\label{exercise:rise_time}
This exercise will introduce you to rise time and overshoot and give you practice setting up the oscilloscope trigger to easily capture an event. Rise time is an important metric when evaluating a signal--especially a digital one. The time it takes for a signal to go from a low value to a high value limits the maximum data transfer rate. On the other hand, when rise time is very short so the signal transitions very quickly, this can cause electromagnetic radiation to interfere with surrounding electronics. 
\par
Rise time is typically defined as the amount of time it takes for a signal to go from $10\%$ of its final value to $90\%$ of its final value. Overshoot is the maximum amount the signal extends past its final value; the percent overshoot is given by $\frac{V_{max}-V_{final}}{V_{final}}$ where $V_{final}$ is the final, settled value.
\begin{itemize}
    \item Build the circuit shown in Figure \ref{label:scope_rise_time_circuit}, where V1 is one of the Waveform Generator channels. Set that waveform generator channel to ``Pulse'' with a frequency of $500\ mHz$ and an amplitude of $5\ V$. 
    
    \begin{figure}[!ht]
    	\centering
    	\includegraphics[width=.4\textwidth]{images/Scope_exercise_rise_time_circuit.png}
    	\caption{Scope Rise Time Exercise Circuit}
    	\centering
    	\label{label:scope_rise_time_circuit}
    \end{figure}
    
    \item Use the oscilloscope to measure across C1--i.e., the channel positive should go to the top node C1 and the channel negative should go to ground. Start the signal generator. 
    \item Set up the scope to trigger off the rising edge of the pulse--you will need to play around with the trigger level, time base and position settings to get a good capture. $1\ ms/div$ time base and $2\ V$ trigger level is a good starting point. Also make sure your vertical scale is a good value for a $5\ V$ signal
    \item Once you have the position, scale, and trigger set up properly, you should see something similar to the waveform shown in Figure \ref{label:scope_rise_time}. It is recommended you do a single-shot capture; single-shot capture is necessary when doing transient analysis on things like power supplies or when looking for electrical bugs, and it is a good idea to get familiar with how it works. 
    \item Now measure the rise time. For our $5\ V$ signal, this will be the time it takes for the signal to go from $(0.1)(5\ V) = 0.5\ V$ to $(0.9)(5\ V) = 4.5\ V$. Note that the series resistance of the inductor plays a large role in the overshoot, rise time, and settling time. The inductor used to capture Figure \ref{exercise:rise_time} has a rather large series resistance of $175\ \Omega$. If yours is significantly different, then the plot and values will also be different. You can continue to complete this exercise with your components, but if you would like to compare results then you can add a resistor in series with your inductor to match $175\ \Omega$. The rise time for this inductor and capacitor is about $250\ \mu s$.
    \item Finally, measure the overshoot. With this inductor and capacitor, the overshoot is about $48\%$.
\end{itemize}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{images/scope_exercise_3.1.png}
	\caption{Example of Properly Setup Scope View for Exercise \ref{exercise:rise_time}}
	\centering
	\label{label:scope_rise_time}
\end{figure}

\newpage
\subsubsection{Observing PWM (Pulse Width Modulation)}
This exercise will give you a chance to build up an op-amp circuit and visually observe how a PWM signal works. A PWM signal is a square wave that spends a certain percent of its period HIGH and the other percent LOW. The major point of a PWM signal is that the percent time spent HIGH and LOW (known as the \textit{duty cycle}) can be precisely set. 
\par
PWM is ubiquitous in electronics design. It's used in the efficient Class-D audio amplifier, motor speed control, LED dimming, servo control, communications, switch-mode power supplies, or as an ADC (analog to digital converter). 
\par
Generally, the PWM signal would be generated digitally. Digital generation is typically cheaper, more robust, and more versatile than generating it with analog circuitry. However, we will use an op-amp circuit to generate the signal for two reasons: first, all the components are included in your EE201/EE230 kit; second, the triangle wave comparator used here is the building block for switch-mode power supplies and class-D amplifiers--both of which are prevalent in nearly every modern consumer gizmo.  

\begin{itemize}
    \item Build the circuit in Figure \ref{label:exercise:scope_exercise_pwm}. Either the LM324 and LMC660 from your EE201/230 kit will work for this circuit. Use the DAD's $\pm5\ V$ power supply to power the circuit.
    
    \begin{figure}[!ht]
    	\centering
    	\includegraphics[width=1\textwidth]{images/scope_exercise_pwm.png}
    	\caption{PWM Exercise Circuit}
    	\centering
    	\label{label:exercise:scope_exercise_pwm}
    \end{figure}
    
    \begingroup \itshape \rightskip\leftmargin
    This circuit is considerably more complicated than any other in this manual, so it deserves a brief explanation. The first circuit block, the square wave generator, is responsible for generating a (roughly) $50\%$ duty cycle square wave. The frequency is set by R1 and C1 and should be around $1\ kHz$. While this block does output a square wave similar to that of a PWM signal, the duty cycle is not easily adjustable.
    \par
    The integrator turns the square wave into a triangle wave. If you think of one small section of the square wave as being a constant (either constant $5\ V$ or constant $-5\ V$), the integral of that constant would be a slope. The magnitude of the slope is set by $\frac{1}{2R_4C_2}$. 
    \par
    Lastly, the comparator compares the current value of the triangle wave to a reference--in this case, the DC value set by the potentiometer R6. When the triangle is below the reference, the output of U3 is LOW ($-5\ V$).
    When the triangle is above the reference, the output of U3 is HIGH ($5\ V$). Figure \ref{label:exercise:scope_exercise_pwm_explination} visually shows this process with the DC set point at $-2\ V$. 
    Note that you could feed the inverting terminal of U3 with an AC waveform and you would get a PWM representation of that AC signal, as long as it is of sufficiently lower frequency than the triangle wave (the Nyquist frequency)--this is the working principle of a class-D amplifier. 
    \par
    \endgroup
    
    \begin{figure}[!ht]
    	\centering
    	\includegraphics[width=0.9\textwidth]{images/scope_exercise_pwm_explination.png}
    	\caption{PWM Generation Using Triangle Wave Comparator}
    	\centering
    	\label{label:exercise:scope_exercise_pwm_explination}
    \end{figure}
    
    \item As you turn the potentiometer, you should observe the LED changing brightness. This is the result the changing PWM duty cycle and your eyes' \href{https://en.wikipedia.org/wiki/Persistence_of_vision}{persistence of vision}.
    
    \item Connect one of the oscilloscope channel's positive input to the output of the comparator, and connect that channel's negative input to ground. 
    \item Set up the scope to trigger on the rising or falling edge and adjust the vertical and horizontal scales to clearly capture the PWM waveform. As you turn the potentiometer, you should observe the duty cycle changing. An example of the output waveform at $75\%$ duty cycle is shown in Figure \ref{label:exercise:scope_exercise_pwm_example}.
    \item Finally, use the oscilloscope to observe the other nodes of the circuit--particularly the outputs of the square wave generator and the integrator. It is a good idea to use the second scope channel to observe the other nodes so you can see how they relate to each other. 
    
    \begin{figure}[!ht]
    	\centering
    	\includegraphics[width=1\textwidth]{images/scope_exercise_pwm_example.png}
    	\caption{Example of Comparator Output at $75\%$ Duty Cycle}
    	\centering
    	\label{label:exercise:scope_exercise_pwm_example}
    \end{figure}
    \FloatBarrier

\end{itemize}