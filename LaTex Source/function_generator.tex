\section{Function Generator}
The DAD includes a 2-channel arbitrary waveform generator\footnote{An \textit{arbitrary} waveform generator simply means you can specify a set of arbitrary points that draw out a signal shape. In other words, the waveforms are customizeable and not limited to basic shapes like sinusoids, squares, triangles, etc.}. The waveform signal can be broken out in two ways: either directly from the provided ``flywires'' or with Digilent's BNC adaptor. The BNC adaptor improves the bandwidth of the output (i.e.,\ how fast or high frequency the signal can be before it starts to get attenuated). The $-3\ dB$ bandwidth with the provided ``flywires'' is $9\ MHz$, which is plenty fast enough for most labs you will take, and certainly fast enough for all EE 230 labs. If you have not discussed bandwidth or the $-3\ dB$ point yet, don't worry--you will learn about it in EE 230!

\subsection{Configuration Window}
To open the Wavegen workspace, click ``Wavegen'' from either the Welcome window or from the green \textbf{\textcolor{ForestGreen}{+}} icon on the Welcome tab. You should see the Wavegen workspace shown in Figure \ref{label:wavegen}. By default, the Wavegen workspace shows a single channel; enabling the second channel will populate a similar adjustment section under channel one. See section \ref{sec:wavgen_channels} for details. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.9\textwidth]{images/Wavegen.png}
	\caption{Wavegen Default Workspace}
	\centering
	\label{label:wavegen}
\end{figure}

\subsection{Waveform Configuration}
The large preview port displaying the signal shape gives you a preview of what your signal will look like. The y-axis shows the voltage amplitude and the x-axis shows the signal time base. Left of the preview are all of the channel settings. 

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.5\textwidth]{images/Wavegen_setup.png}
	\caption{Wavegen Signal Configuration Options}
	\centering
	\label{label:wavegen_setup}
\end{figure}

These will be the most frequently used settings in the Wavegen workspace. The top of the configuration section is where you can run or stop the signal. Clicking \textit{Run} will automatically check the \textit{Enable} check box, enabling the channel output and outputting the waveform on the channel's pin. The difference between running and enabled is specific to some of the more advanced features of the Wavegen instrument. You can read about these features in the official \href{https://s3-us-west-2.amazonaws.com/digilent/resources/instrumentation/waveforms/waveforms-2015_rm.pdf#page10}{Waveforms Reference Guide}. 
\par
Next to the enable button is a dropdown menu that changes the available waveform settings. ``Simple'' gives you all of the basics you need to set up a waveform. If you prefer, you can switch to ``Basic'' which gives you a few more options and a different interface, shown in Figure \ref{label:wavegen_setup_basic}. 
\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.5\textwidth]{images/Wavegen_setup_basic.png}
	\caption{Wavegen Signal ``Basic'' Configuration Options}
	\centering
	\label{label:wavegen_setup_basic}
\end{figure}
\FloatBarrier

The other options expand the feature set of the waveform generator. You can define custom waveform shapes, generate specific finite patterns, generate chirps, and generate modulated signals like those used in AM and FM radio. 
\subsubsection{Parameters}
\begin{itemize}
    \item The frequency and period are related by $f = \frac{1}{T}$, so you will only set one
    \item The amplitude is the maximum output swing (in other words, it is twice the peak-to-peak voltage)
    \item Offset applies a DC offset to the signal, shifting it up or down. Note that the maximum output voltage is $\pm 5\ V$, so if you observe clipping make sure you are not hitting this limit
    \item Symmetry adjusts where the peak of the wave occurs in the cycle
    \item The phase sets the relative offset from either a defined trigger or from the other channel
\end{itemize}

\subsection{Channel 2} \label{sec:wavgen_channels}
To enable channel 2 on the waveform generator, click the ``Channels'' dropdown menu near the top of the Wavegen workspace and check ``Channel 2''.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.6\textwidth]{images/Wavegen_channels.png}
	\caption{Wavegen Channels Toolbar}
	\centering
	\label{label:wavegen_channels}
\end{figure}

This checkbox will populate a second channel with a preview and configuration identical to the first channel. 
\par
The dropdown menu with the default ``No synchronization'' option is used to trigger the outputs of the wavegen and synchronize the two channels' outputs. For example, you can trigger off a signal on the oscilloscope such that, when the scope sees a rising edge, the indicated wavegen channel will output a triangle wave (or whatever you define) for a defined amount of time. This can be handy for debugging or prototyping, but will not see much use in lab courses.
\par
To synchronize the outputs such that they are in phase, set the dropdown menu to ``Synchronized'' and make sure ``Trigger'' is set to ``None'' and ``Run'' is set to ``continuous''. 

\subsection{Exercise}
This exercise will allow you to view the effects of different parameters by connecting the wavegen to an LED. The output current limit for the wavegen is $10\ mA$, so we need to choose resistors that will limit the LED current below $10\ mA$. A $680\ \Omega$ current limiting resistor gives a maximum current of about $5\ mA$ (depending on the LED) at $5\ V$ output, which is more than sufficient headroom. 

\begin{itemize}

\item 
Breadboard the circuit shown in Figure \ref{label:wavgen_circuit}.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=0.8\textwidth]{images/Wavegen_exercise_schematic.png}
	\caption{Wavegen Exercise Circuit}
	\centering
	\label{label:wavgen_circuit}
\end{figure}

\item
Synchronize the outputs by choosing ``Synchronized'' in the Channels toolbar (details in section \ref{sec:wavgen_channels}). Set both channels' frequency to $1\ Hz$ and amplitude to $5\ V$. Enable the channels' outputs by clicking ``Run All'' in the Channels toolbar. You should see both LEDs blinking in sync.
\item
Observe how the LEDs are only on for half of the cycle. This is because they are diodes and only conduct in one direction. To see the full cycle, set the amplitude to $2.5\ V$ and the Offset to $2.5\ V$. This will DC shift the sine wave so that it is oscillating between $0\ V$ and $5\ V$. Note that this does not take into account the forward voltage of the LED (which is dependent upon, among other things, the color). You should try experimenting with the amplitude and DC offset to get smooth, symmetric blinking. 
\item
Next, play with the phase of one of the channels. As you adjust the phase, you'll notice the LEDs are no longer in sync. If you use a phase of $180^{\circ}$, the LEDs should appear to be blinking back and forth. 
\item
Finally, try out the different available waveforms and see how the LED changes. It might be a good idea to keep one channel on a sine or square wave as a reference for how the LED responds to the different signals. 
\end{itemize}

